from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login,logout
from django.forms.models import model_to_dict
from django.utils import timezone
# Create your views here.

from .models import GroceryItem,EventItem
from .forms import LoginForm, AddItemForm, UpdateItemForm, RegisterForm
from .forms import AddEventForm, UpdateEventForm, UpdateProfileForm


def index(request):
	groceryitem_list = GroceryItem.objects.filter(user_id = request.user.id)
	eventitem_list = EventItem.objects.filter(user_id = request.user.id)
	context = {
		'groceryitem_list': groceryitem_list,
		'eventitem_list' : eventitem_list,
		'user': request.user
	}
	return render(request, "django_practice/index.html",context)

def groceryitem(request,groceryitem_id):
	groceryitem = get_object_or_404(GroceryItem,pk=groceryitem_id)

	return render (request, "django_practice/groceryitem.html", model_to_dict(groceryitem))

def eventitem(request,eventitem_id):
	eventitem = get_object_or_404(EventItem,pk=eventitem_id)

	return render (request, "django_practice/eventitem.html", model_to_dict(eventitem))



def register(request):
	
	context = {}
	if request.method == 'POST':
		form = RegisterForm(request.POST)
		if form.is_valid() == False:
			form = RegisterForm()
		else:
			user = User()
			username = form.cleaned_data['username']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			is_staff = False
			is_active = True 

			duplicates = User.objects.filter(username = username)

			if not duplicates:
				
				User.objects.create(username = username, first_name = first_name, last_name = last_name, email = email, is_staff = is_staff , is_active = is_active , password = password)

				authenticated_user = User.objects.get(username = username)
				authenticated_user.set_password(password)
				authenticated_user.save()

		
				return redirect("django_practice:login")
			else:
				context = {
					"error": True
				}
	return render(request, "django_practice/register.html", context)


def change_password(request):


	user = authenticate(username="alexbuena", password="alexa1")
	print(user)

	if user is None:
		authenticated_user = User.objects.get(username="johndoe")
		authenticated_user.set_password('johndoe1')
		authenticated_user.save()
		is_user_authenticated = True

	context = {
		'user':request.user
	}

	return render(request, "django_practice/change_password.html", context)

def login_view(request):
	context = {}
	
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid() == False:
			form = LoginForm()
		else:

			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			
			user = authenticate(username=username, password=password)
			print(user)
				
			context = {
				"username": username,
				"password": password
			}
	
			if user is not None:
				login(request,user)
				return redirect("django_practice:index")
			else:
				context = {
					"error": True
				}
	return render(request, "django_practice/login.html", context)

def logout_view(request):
	logout(request)
	return redirect("django_practice:login")

def update_profile(request):

	usern = User.objects.filter(id = request.user.id)
	context ={
		# "user" : user.request,
		"username" : usern[0].username,
		"first_name": usern[0].first_name,
		"last_name": usern[0].last_name,
		"password": usern[0].password,
		"email": usern[0].email
	}

	if request.method == 'POST':
		form = UpdateProfileForm(request.POST)
		if form.is_valid() == False:
			form = UpdateProfileForm()
		else:
			
			username = form.cleaned_data['username']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			password = form.cleaned_data['password']
			email = form.cleaned_data['email']

			if usern:
				usern[0].username = username
				usern[0].first_name = first_name
				usern[0].last_name = last_name
				usern[0].email = email
				# usern[0].password = password
				usern[0].set_password(password)
				usern[0].save()

				return redirect("django_practice:index")
			else:
				context ={
					"error": True
				}
	return render(request, "django_practice/update_profile.html", context)

def add_item(request):
	context = {}
	if request.method == 'POST':
		form = AddItemForm(request.POST)
		if form.is_valid() == False:
			form = AddItemForm()
		else:
			item_name = form.cleaned_data['item_name']
			category = form.cleaned_data['category']

			duplicates = GroceryItem.objects.filter(item_name = item_name)

			if not duplicates:
				GroceryItem.objects.create(item_name = item_name, category = category, date_created = timezone.now(),user_id=request.user.id)
				return redirect("django_practice:index")
			else:
				context = {
					"error": True
				}
	return render(request, "django_practice/add_item.html", context)

def add_event(request):
	context = {}
	if request.method == 'POST':
		form = AddEventForm(request.POST)
		if form.is_valid() == False:
			form = AddEventForm()
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			date_input = form.cleaned_data['date_input']

			duplicates = EventItem.objects.filter(event_name = event_name)

			if not duplicates:
				EventItem.objects.create(event_name = event_name, description = description, date_created = timezone.now(), date_input = date_input,user_id=request.user.id)
				return redirect("django_practice:index")
			else:
				context = {
					"error": True
				}
	return render(request, "django_practice/add_event.html", context)

def update_item(request, groceryitem_id):
	groceryitem = GroceryItem.objects.filter(pk=groceryitem_id)

	context ={
		"user": request.user,
		"groceryitem_id": groceryitem_id,
		"item_name": groceryitem[0].item_name,
		"category": groceryitem[0].category,
		"status": groceryitem[0].status
	}
	if request.method == 'POST':
		form = UpdateItemForm(request.POST)
		if form.is_valid() == False:
			form = UpdateItemForm()
		else:
			item_name = form.cleaned_data['item_name']
			category = form.cleaned_data['category']
			status = form.cleaned_data['status']

			if groceryitem:
				groceryitem[0].item_name = item_name
				groceryitem[0].category = category
				groceryitem[0].status = status

				groceryitem[0].save()
				return redirect("django_practice:index")

			else:
				context ={
					"error": True
				}
	return render(request, "django_practice/update_item.html", context)

def update_event(request, eventitem_id):
	eventitem = EventItem.objects.filter(pk=eventitem_id)

	context ={
		"user": request.user,
		"eventitem_id": eventitem_id,
		"event_name": eventitem[0].event_name,
		"description": eventitem[0].description,
		"status": eventitem[0].status,
		"date_input" : eventitem[0].date_input
	}

	if request.method == 'POST':
		form = UpdateEventForm(request.POST)
		if form.is_valid() == False:
			form = UpdateEventForm()
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']
			# date_input = form.cleaned_data['date_input']

			if eventitem:
				eventitem[0].event_name = event_name
				eventitem[0].description = description
				eventitem[0].status = status
				# eventitem[0].date_input = date_input

				eventitem[0].save()
				return redirect("django_practice:index")

			else:
				context ={
					"error": True
				}
	return render(request, "django_practice/update_event.html", context)

def delete_item(request, groceryitem_id):
	groceryitem = GroceryItem.objects.filter(pk=groceryitem_id).delete()
	return redirect("django_practice:index")

def delete_event(request, eventitem_id):
	eventitem = EventItem.objects.filter(pk=eventitem_id).delete()
	return redirect("django_practice:index")


