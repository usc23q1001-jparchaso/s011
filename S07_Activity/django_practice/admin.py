from django.contrib import admin

# Register your models here.
from .models import GroceryItem,EventItem

admin.site.register(GroceryItem)
admin.site.register(EventItem)
